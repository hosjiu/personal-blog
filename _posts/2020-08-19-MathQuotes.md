---
layout: post
comments: false
title: "My fav Math Quote"
excerpt: "Trích dẫn ưa thích của tôi về toán học."
---
> No one shall expell us from the paradise that Cantor created for us - **David Hilbert**

<div>
	<p align="center">
		<img src="/personal-blog/assets/hilbert_quote.jpeg">
	</p>
</div>

_Tôi có một niềm đam mê nhất định đối với toán học ngay từ khi còn ngồi trên ghế nhà trường. Dạo gần đây, tôi đã dành thời gian để đọc một quyển sách rất hay về định lý cuối cùng nhà toán học của Fermat hay còn có một cái tên mỹ miều khác là "Định lý cuối cùng của Fermat" - do **Simon Singh** dịch_.

_Trong đó, tôi đã bắt gặp một câu nói rất hay của một trong những nhà toán học mà tôi rất ấn tượng, đó là David Hilbert. Ông ấy sống cùng thời với Georg Cantor và là một nhà toán học có thực lực cũng nhữ tiếng nói trong giới toán học thời ấy. Như mọi người đều biết, Cantor là một nhà lý thuyết tập hợp và là người đã tạo ra cái gọi là lý thuyết tập hợp Cantor cho các tập hợp vô hạn. Nghiệt ngã thay, ông lại bị chính người thầy của mình - cũng là một nhà toán học khác - tên là Kronecker *đì* và mãi cho đến về sau, khi mà ông đã mất, thì mới được các nhà toán học công nhận. Và một trong số đó có Hilbert. Và để tưởng nhớ đến các công trình mà Cantor đã xây dựng. Ông ấy đã có một câu nói như này:_

> Không ai có thể kéo chúng ta ra khỏi cái thiên đường mà Cantor đã tạo ra cho chúng ta.

_Ngoài ra, ông cũng đã từng trình bày về một câu chuyện trong một bài giảng của ông (không được xuất bản) mà sau này đã trở thành một trong những nghịch lý (paradox) nổi tiếng. Đó là [nghịch lý khách sạn vô hạn của Hilbert](https://en.wikipedia.org/wiki/Hilbert%27s_paradox_of_the_Grand_Hotel)._