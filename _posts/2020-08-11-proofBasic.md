---
layout: post
comments: true
mathjax: true
title: "A naive proof for Number of Subsets of A Set problem"
excerpt: "Một chứng minh *naive* của tôi cho một vấn đề nhỏ trong lý thuyết tập hợp"
---
<div>
	<p align="center"><img src="/personal-blog/assets/Georg_Cantor.jpeg"></p>
	<div>
		<p align="center">
			<a href="https://en.wikipedia.org/wiki/Georg_Cantor" target="_blank">Georg Cantor (wikipedia)</a>
		</p>
	</div>
</div>

Tôi đã đọc qua một [bài viết](https://medium.com/cantors-paradise/proving-cantors-theorem-dbfbc4a37f56) trên medium về một chứng minh của nhà toán học nổi tiếng [Georg Cantor](https://en.wikipedia.org/wiki/Georg_Cantor) cho một định lý của ông. Trong quá trình đọc, tôi đã bắt gặp một phát biểu như thế này từ tác giả:

> If a set has N elements, then the set of subsets has $$2^{n}$$ elements

Tạm hiểu là:

> Cho một tập hợp có N phần tử, thì ta có tập hợp chứa tất cả các tập hợp con của nó sẽ có số lượng phần từ là $$2^{n}$$ _(*)_

Đây là một ví dụ mà tác giả đã đưa ra để chúng ta dễ hình dung:

> Chẳng hạn, tập hợp $$A = $${1,2,3} có 3 phần tử là 1, 2 và 3. Nó có 8 tập hợp con: {1}, {2}, {3}, {1,2}, {2,3}, {1,3}, {} và {1,2,3} \\
Và rõ ràng là 8 = $$2^{3}$$ (ở đây **3** là số phần tử của tập hợp A)

Tôi đã muốn tự chứng minh lại điều này ngay khi đọc qua nó. Và dưới đây là một chứng minh của nó mà tôi đã thử.

## Proof
> Tôi sẽ chứng minh phát biểu _(*)_ đúng với các phần từ của một tập hợp là các số thực $$R$$

Xét một tập hơp $$C$$ bất kỳ với các phần tử $$c_{i}$$ là các số thực $$R$$ hay viết dưới ký hiệu toán học sẽ là $$ C = \lbrace c_{1}, ... , c_{n}\rbrace $$ trong đó $$c_{i} \in R$$, $$\forall i = 1 .. n$$

Gọi $$S_{i}$$ _($$i = 0 .. n $$)_ là tập hợp mà các phần tử của nó là các tập hợp $$s_{k}$$ có số lượng phần tử - lực lượng _([Cardinality](https://en.wikipedia.org/wiki/Cardinality))_ - bằng $$i$$ và các phần tử của nó (tức các tập hợp $$s_{k}$$) được lấy ra từ tập hợp $$C$$ ở trên.

Ta có thể xét thử trường hợp $$ i = 1 $$ để dễ dàng thấy:
\begin{equation}
S_{1} = \lbrace \lbrace c_{1} \rbrace, \lbrace c_{2} \rbrace, \lbrace c_{3} \rbrace, \ldots, \lbrace c_{n} \rbrace \rbrace
\end{equation}

Dễ thấy rằng
\begin{equation}
\vert S_{1} \vert = \binom{n}{1}
\end{equation}

Tổng quát hoá cho mọi giá trị của $$i$$, ta có:
\begin{equation}
\vert S_{i} \vert = \binom{n}{i}
\end{equation}

Gọi $$S$$ là tập hợp chứa tất cả các tập hợp con của $$C$$ _(bao gồm tập rỗng và cả chính tập $$C$$)_. Rõ ràng, cardinality của $$S$$ bằng tổng các cardinality của $$S_{i}, \forall i = 0 .. n$$

Do đó:

$$
\begin{align}
\vert S \vert &= \vert S_{0} \vert + \vert S_{1} \vert + ... \vert S_{n} \vert \\ \\
\vert S \vert &= \binom{n}{0} + \binom{n}{1} + \ldots + \binom{n}{n} \tag{1} \\ \\ 
\vert S \vert &= 2^{n} \quad (đ.p.c.m) \quad \blacksquare
\end{align}
$$

Ở trên là chứng minh khá cơ bản của tôi. Nếu như có gì không hiểu hoặc thiếu sót, các bạn có thể comment ở dưới bài blog này để chúng ta cùng trao đổi thêm.

### Appendix
* Giái thích kết quả của biểu thức $$(1)$$

Theo [định lý nhị thức - Binomial theorem](https://en.wikipedia.org/wiki/Binomial_theorem) chúng ta luôn có:

\begin{equation}
(x + y)^{n} = \sum_{k=0}^n\binom{k}{n}x^{n-k}y^{k}
\end{equation}

Hay nếu khai triển rõ ra:

\begin{equation}
(x + y)^{n} = \binom{n}{0}x^{n}y^{0} + \binom{n}{1}x^{n-1}y^{1} + \ldots + \binom{n}{n-1}x^{1}y^{n-1} + \binom{n}{n}x^{0}y^{n} \tag{*}
\end{equation}

Chọn $$x = 1$$, $$y = 1$$ và thay vào biểu thức $$(*)$$, thu được:

$$
\begin{align}
(1 + 1)^{n} &= \binom{n}{0} + \binom{n}{1} + \ldots + \binom{n}{n} \\ \\
2^n &= \binom{n}{0} + \binom{n}{1} + \ldots + \binom{n}{n}
\end{align}
$$

## References:
* [https://medium.com/cantors-paradise/proving-cantors-theorem-dbfbc4a37f56](https://medium.com/cantors-paradise/proving-cantors-theorem-dbfbc4a37f56)
* [https://en.wikipedia.org/wiki/Binomial_theorem](https://en.wikipedia.org/wiki/Binomial_theorem)











