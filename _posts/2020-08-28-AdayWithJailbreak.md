---
layout: post
comments: true
title: "Playground with iOS jailbreaking"
excerpt: "Một chút trải nghiệm của tôi với jailbreaking."
---
### Dẫn Nhập
Tôi có một chiếc ipad cũ (thế hệ thứ 1) - đây là thế hệ đầu tiên của dòng Ipad và được ra mắt cách đây cũng gần 10 năm (tính đến thời điểm tôi viết bài này).

Dạo gần đây, tôi hay đọc sách nên đã muốn dùng đến con Ipad này để tải sách ebook (PDF, epub, ...) về đọc cho tiện.
Tuy nhiên, do máy không có sẵn *ibook* và một phần nó cũng đang chạy hệ điều hành *iOS 5.1.1* (quá lỗi thời) nên tôi không thể lên *App Store* của Apple để tải vể được (Lỗi không tương thích OS). Vì thế tôi đã quyết định search google giải pháp và quyết định jailbreak chiêc Ipad này. Do không có nhiều thời gian nên tôi chỉ nghiên cứu qua loa và sau cùng đã tìm được một cách thức để jailbreak nó.

#### Mục đích của việc jailbreaking
> Cố gắng loại bỏ đi những rào cản vốn được đặt ra bởi các nhà phát triển phần cứng. Như trong trường hợp của tôi là muốn được tải một ứng dụng - trình đọc và quản lý file - không nằm trong App Store (đây là nơi duy nhất bạn có thể tải được ứng dụng ở hệ điều hành iOS và điều này là bắt buộc nếu bạn không jailbreak).

#### Jailbreaking là gì?
> Nôm na, đó là một cách để người dùng (chủ yếu là các hacker có năng lực cả về hardware lẫn software) tác động vào kernel bên trong của một thiết bị phần cứng trong quá trình nó khởi động. (Hard way to understand?)

> Theo như wikipedia có giải thích thì kernel là một chương trình máy tính tại lõi của một hệ điều hành máy tính với khả năng điều khiển mọi thứ trong hệ thống máy tính đó.một thứ nằm giữa các phần cứng và phần mềm (hình dưới).
Nó chịu trách nhiệm trung gian cho việc "liên lạc" giữa các ứng dụng (applicaiton) với ngoại vi (CPU, Memory, Screen, ...). Như vậy, nó sẽ chịu trách nhiệm chính cho việc điều phối hoạt động của một ứng dụng. Ứng dụng có được chạy hay không? - cũng phải thông qua Kernel, bắt buộc. Vì là thứ quan trọng trong việc chi phối những ứng dụng của người dùng và ứng dụng của hệ thống nên Kernel thường sẽ được load đầu tiên ngay khi thiết bị được khởi động (ở đây thiết bị của chúng ta là chiêc Ipad của tôi). Theo như cách hiểu của tôi khi đọc trên wikipedia, thường thì jailbreaking sẽ lợi dụng các lỗ hổng (vulnerabiliies) trong phần mã được boot lên của kernel ở giai đoạn đầu khi boot và sẽ tác động vào nó (bằng cách nào đó) để thay đổi đi cách thực hoạt động thực sự của Kernel. Hay nói một cách dễ hiểu là chúng ta đang làm thay đổi hành vi vốn có của thiêt bị mà các nhà phát triển mong đợi.

<div>
	<p align="center">
		<img src="/personal-blog/assets/kernel_layout.svg"/>
	</p>
</div>

Okay, chúng ta vừa nói sơ qua một chút về **jailbreaking**.

Đây là một số bước và hình ảnh trong quá trình tôi thực hiện việc jailbreaking trên chiếc Ipad 1:

- Lúc đầu, tôi thực hiện việc tải một số phần mềm cần thiết và nó yêu cầu hệ điều hành [Windows XP](https://en.wikipedia.org/wiki/Windows_XP). Nhưng do việc cài bộ khởi động từ USB không thành công nên tôi phải đành dùng VMware như một giải pháp khắc phục và nó hoạt động khá trơ tru trên file ISO Windows XP mà tôi tải trước đó.

<div>
	<p align="center">
		<img src="/personal-blog/assets/jailbreak_step1.jpeg" style="width: 30vw; min-width: 330px"/>
	</p>
	<p align="center">
		Trong quá trình cài đặt Windows XP
	</p>
</div>

- Sau khi đó có Windows XP trong tay, tôi bắt đầu tải các phần mềm liên quan khác và thực hiện jailbreaking. Có khá nhiều cách thức jailbreaking khác nhau (tôi không quá rõ), nhưng giải pháp mà tôi chọn tương đối đơn giản để thực hiện. Về cơ bản thì chỉ cần kết nối Ipad với máy tính và chạy chương trình theo hướng dẫn của tác giả. Đây là hình ảnh sau khi tôi đã jailbreaking xong và đã có trong tay [Cydia](https://en.wikipedia.org/wiki/Cydia). Đây là một trình quản lý package (package manager) cho các thiết bị chạy iOS để cài đặt các ứng dụng *không chính thống*.
Ban đầu tôi cần ibook để có thể đọc và lưu các tập tin PDF. Nhưng sau một hồi không tìm thấy ibook trong kho ứng dụng của Cydia, thì có một ứng dụng khác hoạt động *gần như tương tự* ibook - tuy theo một cách không thân thiện với những người dùng bình thường.

<div>
	<p align="center">
		<img src="/personal-blog/assets/jailbreak_step2.jpeg" style="width: 30vw; min-width: 330px"/>
	</p>
	<p align="center">
		Cài một ứng dụng từ <a href="https://en.wikipedia.org/wiki/Cydia">Cydia</a>
	</p>
</div>

> Tôi đã thực sự thích cách tiếp cận của ứng dụng này. Đứng trên phương diện là một coder, tôi đã bị kích thích khi có thể tác động vào filesystem của chiếc Ipad thông qua giao thức tryền phải bảo mật [SFTP](https://en.wikipedia.org/wiki/SSH_File_Transfer_Protocol) (SSH File Transfer Protocol) - sử dụng [SSH](https://en.wikipedia.org/wiki/Secure_Shell) như một phương thức bảo mật thông tin.

- Đây là thời điểm tôi sử dụng [Cyberduck](https://cyberduck.io/) để truy cập vào filesystem của Ipad 1 và truyền toàn bộ sách từ chiếc Macbook của tôi đến nó. Một khoảnh khắc khó tả

<div>
	<p align="center">
		<img src="/personal-blog/assets/jailbreak_step3.jpg" style="width: 30vw; min-width: 330px"/>
	</p>
	<p align="center">
		Chuyển sách trong một niềm hân hoan
	</p>
</div>

<br>
<br>
<br>
<br>

<div>
	<p align="center">
		<img src="/personal-blog/assets/jailbreak_step4.jpeg" style="width: 30vw; min-width: 330px"/>
	</p>
	<p align="center">
		Toàn bộ hệ thống file của chiếc Ipad 1
	</p>
</div>


### Sau cùng

Giờ thì tôi cũng đã có thể sử dụng chiếc Ipad để đọc những quyển sách yêu thích của mình. Nhưng vấn đề hơn những thế, đó là, iOS jailbreaking thực sự đã cho tôi những trải nhiệm thú vị về mặt kỹ thuật.

