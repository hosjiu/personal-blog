---
layout: post
comments: true
mathjax: true
title: "Một Chứng minh cho định lý Euclid của nhà toán học người Hungary - Paul Erdős"
excerpt: "Bài viết này trình bày một chứng minh khác của Erdős cho định lý Euclid về các số nguyên tố."
---
*(Status: __Draft__)*

___

> Bài viết được lấy cảm hứng từ [Erdős’ Proof of the Infinitude of Primes](https://medium.com/cantors-paradise/erd%C5%91s-proof-of-the-infinitude-of-prime-eb6ab6ce37c8) trên Medium.
Ý tưởng chính của lời giải được Paul cùng các đồng nghiệp viết trong quyển sách [Proofs from THE BOOK](https://en.wikipedia.org/wiki/Proofs_from_THE_BOOK), được xuất bản lần đầu tiên vào năm 1998.

__Ngoài lề__: Paul Erdos đã mất trước đó vài năm khi quyển sách còn chưa được xuất bản chính thức.

> Để hiểu được đầy đủ lời giải - tuy có phần sơ cấp này - một cách đầy đủ, tôi đã phải đi thông qua một vài tài liệu ở trên Internet và tất cả đều được đính kèm link ở cuối bài viết.

#### I. Phát biểu định lý Euclid:
> Định lý của Euclid là một mệnh đề cơ bản trong lý thuyết số khẳng định rằng **số lượng các số nguyên tố là vô hạn.**

Euclid có một chứng minh cho định lý này trong quyển **Elements** của ông ấy và có lẽ là tôi sẽ để dành nó trong một bài viết khác.

#### II. Chứng minh của Erdős:
Chứng minh của Erdős về cơ bản là dựa trên tính mâu thuẫn và hơn nữa là ông ấy đã sử dụng một kết quả của Euler trước đây. Đó là, __tổng nghịch đảo của các số nguyên tố phân kỳ__

\begin{equation}
\sum_{p \text{ prime}} \frac{1}{p} = \frac{1}{2} + \frac{1}{3} + \frac{1}{5} + \frac{1}{7} + \ldots = \infty
\end{equation}

**Proof:**

Xét dãy các số nguyên tố tăng dần sau đây $$ p_{1}, p_{2}, p_{3}, \ldots $$ và giả định rằng tổng nghịch đảo $$ \sum_{p \text{ prime}}\frac{1}{p} $$ hội tụ.

Một cách tường minh, đó là:

\begin{equation}
\frac{1}{p_{1}} + \frac{1}{p_{2}} + \frac{1}{p_{3}} + \ldots = L < \infty
\end{equation}

Do đó, tồn tại một số nguyên dương $$k$$ sao cho $$ \sum_{i \ge k+1}\frac{1}{p_{i}} < \frac{1}{2}$$

Gọi $$ p_{1}, p_{2}, p_{3}, \ldots, p_{k} $$ là tập hợp các số *nguyên tố nhỏ* \
Gọi $$ p_{k+1}, p_{k+2}, p_{k+3}, \ldots $$ là tập hợp các số *nguyên tố lớn*

Xét 3 tập hợp: \
$$ S = \lbrace 1, 2, 3, \ldots, N \rbrace $$ (trong đó N là một số nguyên dương bất kỳ) \
$$ A = \lbrace s \in S \text{ }| \text{ s chỉ có duy nhất các ước số thuộc tập hợp các số nguyên tố nhỏ} \rbrace $$ \
$$ B = \lbrace s \in S \text{ }| \text{ s có ít nhất một ước thuộc tập hợp các số nguyên tố lớn} \rbrace $$ \

Ta có:\
$$ \vert S \vert = N $$
$$ A \cap B = $$





