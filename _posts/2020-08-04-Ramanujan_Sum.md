---
layout: post
comments: true
mathjax: true
title: "Tản mạn về tổng vô hạn 1 + 2 + 3 + 4 + ..."
excerpt: "Một chút tản mạn về tổng Ramanujan"
---
<div>
	<p align="center">
		<img src="/personal-blog/assets/inf.jpg" style="width: 55vw; min-width: 330px;"/>
	</p>
	<p align="center">
		Credit: <a href="https://unsplash.com/photos/XDNiTGp3bKM">unsplash</a>
	</p>
</div>
> Blog post này tôi viết sau khi đọc qua một post thú vị trên [Medium](https://medium.com/cantors-paradise/the-ramanujan-summation-1-2-3-1-12-a8cc23dea793).

Xét tổng $$S$$ như sau:
\begin{equation}
S = 1 + 2 + 3 + 4 +\ldots \tag{1}
\end{equation}

Chúng ta sẽ thực hiện một vài biến đổi đại số _"cơ bản"_ và  đi đến với một kết quả khá bất ngờ khi tổng __(1)__ là một số âm và giá trị của nó bằng $$-\frac{1}{12}$$. Tức là:
\begin{equation}
1 + 2 + 3 + 4 +\ldots = -\frac{1}{12}
\end{equation}

Bây giờ, chúng ta sẽ bắt đầu chứng minh:

Xét tổng $$S_{1}$$:
\begin{equation}
S_{1} = 1 - 1 + 1 - 1 +\ldots \tag{2.1}
\end{equation}

Ta có:

$$
\begin{align}
1 - S_{1} &= 1 - (1 - 1 + 1 - 1 +\ldots) \\ 
&= 1 - 1 + 1 - 1 +\ldots \\
&= S_{1}
\end{align}
$$

\begin{equation}
\Rightarrow S_{1} = \frac{1}{2} \tag{2.2}
\end{equation}

Giá trị của tổng __(2.1)__ sẽ được dùng để tính một tổng trung gian khác dưới đây.

Ta xét tiếp một tổng $$S_{2}$$ khác:
\begin{equation}
S_{2} = 1 - 2 + 3 - 4 +\ldots
\end{equation}

Ta có:

$$
\begin{align}
S_{2} &= 1 - (2 - 3 + 4 - 5 +\ldots) \\
&= 1 - [(1 + 1) - (1 + 2) + (1 + 3) -\ldots] \\
&= 1 - (1 - 2 + 3 - 4 +\ldots) - (1 - 1 + 1 - 1 +\ldots) \\
&= 1 - S_{2} - S_{1} \\
&= 1 - S_{2} - \frac{1}{2} \tag{do (2.1)}
\end{align}
$$

\begin{equation}
\Rightarrow S_{2} = \frac{1}{4} \tag{3}
\end{equation}

Okay, chúng ta bây giờ sẽ quay ngược trở lại để tính giá trị của $$S$$. Thực ra, có thể đối với một số người điều này có thể không được tự nhiên, nhưng tạm thời chúng ta
sẽ tạm bỏ quá nó ở đây. Trong toán học, đôi khi mọi thứ chỉ là trực giác ...

Thực hiện biến đổi đại số sau đây:
\begin{align}
S - S_{2} &= (1 + 2 + 3 + 4 +\ldots) - (1 - 2 + 3 - 4 +\ldots) \tag{4}
\end{align}

Để cho tiện _*quan sát*_ tôi sẽ xếp chồng hai tổng trên theo chiều dọc như sau:

$$
\begin{align}
(1 + 2 + 3 + 4 +\ldots) \\
- (1 - 2 + 3 - 4 +\ldots)
\end{align}
$$

Tiếp tục ...

$$
\begin{align}
(4)\Leftrightarrow S - S_{2} &= 4 + 8 + 12 +\ldots \\
&= 4(1 + 2 + 3 + 4 +\ldots) \\
&= 4S \\
-S_{2} &= 3S \\
\Rightarrow S &= -\frac{S_{2}}{3} = -\frac{1}{12} \tag{do (3)}
\end{align}
$$

Như vậy chúng ta đi đến kết quả sau cùng của biểu thức lúc đầu. Các bạn nghĩ sao về kết quả __*thú vị*__ này?

Tham khảo:

* [Youtube (Numberphile): ASTOUNDING: 1 + 2 + 3 + 4 + 5 + ... = -1/12](https://www.youtube.com/watch?v=w-I6XTVZXww)
* [Youtue (Numberphile): Why -1/12 is a gold nugget?](https://www.youtube.com/watch?v=0Oazb7IWzbA)
* [wikipedia 1 + 2 + 3 + 4 + ...](https://en.wikipedia.org/wiki/1_%2B_2_%2B_3_%2B_4_%2B_%E2%8B%AF)
* [Terence Tao's nice blog](https://terrytao.wordpress.com/2010/04/10/the-euler-maclaurin-formula-bernoulli-numbers-the-zeta-function-and-real-variable-analytic-continuation/#zeta+2)
