---
layout: post
title: "Deep & Shallow Blogs"
excerpt: "Một số blog hay và thú vị để đọc dần về ML/DL, math, neuroscience, tech và khoa học nói chung."
---
> Post này là nơi lưu trữ một vài blog để tôi đọc dần, về:
> * Math
> * Machine Learning / Deep Learning / Computer Vision / Natural Language Processing
> * Neuroscience
> * Tech hoặc Science nói chung.

* [http://karpathy.github.io/](http://karpathy.github.io/)
* [https://www.inference.vc/page/8/](https://www.inference.vc/page/8/)
* [http://colah.github.io/](http://colah.github.io/)
* [http://iamtrask.github.io/](http://iamtrask.github.io/)
* [https://adeshpande3.github.io/adeshpande3.github.io/](https://adeshpande3.github.io/adeshpande3.github.io/)
* [https://petewarden.com/](https://petewarden.com/)
* [https://blog.otoro.net/archive.html](https://blog.otoro.net/archive.html)
* [https://ruder.io/](https://ruder.io/)
* [https://jeremykun.com/](https://jeremykun.com/)
* [http://timdettmers.com/](http://timdettmers.com/)
* [https://ongxuanhong.wordpress.com/category/data-science/algorithms/](https://ongxuanhong.wordpress.com/category/data-science/algorithms/)
* [https://sebastianraschka.com/blog/index.html](https://sebastianraschka.com/blog/index.html)
* [http://www.wildml.com/](http://www.wildml.com/)
* [http://timvieira.github.io/blog/](http://timvieira.github.io/blog/)
* [http://mccormickml.com/](http://mccormickml.com/)
* [http://neuralnetworksanddeeplearning.com/index.html](http://neuralnetworksanddeeplearning.com/index.html)
* [https://blog.piekniewski.info/](https://blog.piekniewski.info/)
* [https://shuzhanfan.github.io/tags/](https://shuzhanfan.github.io/tags/)
* [https://e2eml.school/blog.html](https://e2eml.school/blog.html)
* [https://www.cs.toronto.edu/~hinton/](https://www.cs.toronto.edu/~hinton/)
* [http://crsouza.com/](http://crsouza.com/)
* [http://evjang.com/](http://evjang.com/)
* [http://kvfrans.com/](http://kvfrans.com/)
* [https://peterroelants.github.io/](https://peterroelants.github.io/)
* [https://www.jeremyjordan.me/](https://www.jeremyjordan.me/)
* [https://neuroecology.wordpress.com/archive](https://neuroecology.wordpress.com/archive)
* [https://neurdiness.wordpress.com/](https://neurdiness.wordpress.com/)
* [https://lilianweng.github.io/lil-log/](https://lilianweng.github.io/lil-log/)
* [https://www.computervisionblog.com/](https://www.computervisionblog.com/)
* [https://guillaumegenthial.github.io/](https://guillaumegenthial.github.io/)
* [https://roberttlange.github.io/year-archive/](https://roberttlange.github.io/year-archive/)
* [https://twiecki.io/](https://twiecki.io/)
* [https://rosetta.vn/](https://rosetta.vn/)
* [https://divisbyzero.com/blog-division-by-zero/](https://divisbyzero.com/blog-division-by-zero/)
* [http://akosiorek.github.io/](http://akosiorek.github.io/)
* [https://www.imomath.com/index.php](https://www.imomath.com/index.php)
* [https://francisbach.com/](https://francisbach.com/)
