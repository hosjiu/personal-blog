---
layout: post
comments: true
mathjax: false
title: "Xin chào, concurrent.futures"
excerpt: "Những trải nghiệm rất cơ bản về concurrent programming với module Concurrent.futures của Python"
---
## Dẫn nhập

Đã bao giờ bạn tự hỏi module [concurrent.futures](https://docs.python.org/3.7/library/concurrent.futures.html#module-concurrent.futures) của ngôn ngữ lập trình Python được sử dụng để làm gì chưa?


Với tôi thì có. Cái gì đó khá *ngầu* và cũng hơi khó hiểu ở cái tên *futures* trong tên của module này. Và đây dường như là module duy nhất trong package [Concurrent](https://docs.python.org/3.7/library/concurrent.html). Trong một số framework nổi bật về học máy hầu như đều có [sử dụng đến](https://github.com/pytorch/pytorch/blob/c371542efc31b1abfe6f388042aa3ab0cef935f2/torch/distributed/elastic/multiprocessing/tail_log.py) module này.


Ok, không dài dòng thêm nữa, chúng ta sẽ đi thẳng trực tiếp vào cách mà module này được sử dụng và tại sao nó lại có liên quan đến [Concurrent Programming](https://en.wikipedia.org/wiki/Concurrent_computing)



## I/O-Bound tasks


Đây là những công việc thông thường liên quan đến Networking, ví dụ như:

* Thực hiện một lời gọi API bên ngoài
* Download file trên Internet

Ngoài ra, các task I/O bound cũng có thể là các việc thường xuất hiện hằng ngày trên một chiếc máy tính bình thường như đọc, ghi, lưu file vào disk cứng của bạn hoặc mấy thứ về stdout, stdin. Một ví dụ đó là câu lệnh `print` trong Python.


## Tiny Example


Cùng xem qua đoạn code dưới đây nào:

```python
def just_sleep(n: int = 3) -> None:
    """ Do nothing on n seconds (I/O bound)
    """
    print(f"Be waiting {n} seconds ...")

    # I/O bound task
    sleep(n)

    print(f"Done.\n")
```

Ở dây, tôi định nghĩa một function có nhiệm vụ chính là cho phép *hoãn chương trình* lại một khoảng thời gian **n** giây trước khi in ra thông điệp có nội dung là "Done." và xuống dòng.

Hãy chạy function này trong một script python đơn giản như dưới đây:

```python
from time import sleep


def just_sleep(n: int = 3) -> None:
    """ Do nothing on n seconds (I/O bound)
    """
    print(f"Be waiting {n} seconds ...")

    # I/O bound task
    sleep(n)

    print(f"Done.\n")


if __name__ == '__main__':
    for _ in range(5):
        # "pause" the program 3s by default
        just_sleep()
```

Về cơ bản, chúng ta đang yêu cầu chương trình tạm dừng **5 lần** và mỗi lần dừng khoảng **3 giây**. Một cách tuyến tính thì chúng ta sẽ phải mất khoảng đâu đó tầm 5 x 3 = 15 (giây) để có cho đoạn script này chạy xong.
Để đảm bảo rằng là tôi đang không mắc lỗi logic gì ở đây thì chúng ta nên thực hiện một công đoạn nhỏ nữa là thêm `time log` vào chương trình này.

```python
# thread_pool1.py

import time
from time import sleep


def just_sleep(n: int = 3) -> None:
    """ Do nothing on n seconds (I/O bound)
    """
    print(f"Be waiting {n} seconds ...")

    # I/O bound task
    sleep(n)

    print(f"Done.\n")


if __name__ == '__main__':
    start_time = time.time()

    for _ in range(5):
        # "pause" the program 3s by default
        just_sleep()

    print("Elapsed time: {:.2f} seconds".format(time.time() - start_time))
```

Cùng chạy thử coi nào.

<div>
    <p align="center">
        <img src="/personal-blog/assets/hi_concurrent1.png">
    </p>
</div>


## Concurrent.futures comes to play

Bạn có thử đặt câu hỏi rằng cách chúng ta có thể là làm gì đó trong quá trình chờ 3s kia được không?

Hãy lấy từ cuộc sống thực nhé, bạn làm gì khi đang đứng chờ xe bus, hoặc chờ tới lượt mình tính tiền trong siêu thị. Có thể là bất cứ thứ gì đúng chứ nhỉ?, lướt fb chẳng hạn, nhưng tôi tin là nó sẽ khiến bạn đỡ thấy chán hơn trong lúc đó. Ở đây cũng vậy, trong lúc `sleep` thì chúng ta hoàn toàn có thể thực hiện một `sleep` khác. Tại sao không khi mà Python hoàn toàn cho phép bạn thực hiện điều đó. 🔥🔥🔥

### Ngoài lề
___

Thực ra, chúng ta không thể thực hiện một lúc nhiều hơn một task tại một thời điểm ở trong Python khi mà cái gọi là GLOBAL INTERPRETER LOCK (GIL) sẽ ngăn chương trình thực thi "song song" nhiều task một lúc.

Về mặt lịch sử, thì Python được thiết kế sao cho nó đảm bảo được được rằng interpreter chỉ thực hiện một task tại một thời điểm ở trên cùng một thread. Tuy nhiên cũng đã có những nỗ lực để loại bỏ GIL ở Python 1.5. Tuy nhiên, [theo Guido van van Rossum](https://www.artima.com/weblogs/viewpost.jsp?thread=214235) (người tạo ra ngôn ngữ lập trình Python) đã có những cảnh báo nhất định về việc loại bỏ hoàn toàn GIL, khi mà nó có những tác động về mặt hiệu năng nhất định.

___


Hãy sử dụng đến Class [TheadPoolExecutor](https://docs.python.org/3.7/library/concurrent.futures.html#threadpoolexecutor) để thực hiện điều mà ta mong muốn, đó là chạy nhiều function `just_sleep` một cách asynchronous.

```python
def future_fn(fn):
    """ Using ThreadPoolExecutor class to
    create a convention thread manager
    """
    with ThreadPoolExecutor(max_workers=5) as executor:
        """
        .map() function in concurrent futures is just the same
        as .map built-in function of Python language

        arg1: fn (callable) to execute
        arg2: an iterable to pass in as argument for the above fn

        """
        executor.map(fn, [3]*5)
```

Ở đây, chúng ta sử dụng hàm [.map()](https://docs.python.org/3.7/library/concurrent.futures.html#concurrent.futures.Executor.map) của class ThreadPoolExecutor, là một subclass của abstract class [concurrent.futures.Executor](https://docs.python.org/3.7/library/concurrent.futures.html#concurrent.futures.Executor)


```python
# thread_pool2.py

import time
from time import sleep
from concurrent.futures import ThreadPoolExecutor


def just_sleep(n: int = 3) -> None:
    """ Do nothing on n seconds (I/O bound)
    """
    print(f"Be waiting {n} seconds ...")

    # I/O bound task
    sleep(n)

    print(f"Done.\n")


def future_fn(fn):
    """ Using ThreadPoolExecutor class to
    create a convention thread manager
    """
    with ThreadPoolExecutor(max_workers=5) as executor:
        """
        .map() function in concurrent futures is just the same
        as .map built-in function of Python language

        arg1: fn (callable) to execute
        arg2: an iterable to pass in as argument for the above fn

        """
        executor.map(fn, [3]*5)


if __name__ == '__main__':
    start_time = time.time()

    # current_fn(just_sleep)
    future_fn(just_sleep)

    print(f"Elapsed time: {time.time() - start_time} seconds")
```


Hãy chạy lại và xem kết quả nào.

<div>
    <p align="center">
        <img src="/personal-blog/assets/hi_concurrent2.png">
    </p>
</div>

**3s!** Đó là thời gian chạy khi sử dụng **ThreadPoolExecutor**

## Some Explaination

Chúng ta sẽ thảo luận một chút về việc tại sao, khi sử dụng module **futures** lại cho kết quả như vậy.

Như tôi đã đề cập ở trên về vấn đề [GIL](https://en.wikipedia.org/wiki/Global_interpreter_lock). Bạn sẽ không thể thực hiện nhiều hơn được một task tại một thời điểm và cách để vượt qua vấn đề này là sử dụng đến cách tiếp cận khác và vượt ra khỏi phạm vi của bài viết này. (*bật mí: sử dụng module [multiprocessing](https://docs.python.org/3/library/multiprocessing.html)*)


Dẫu vậy, với I/O bound task thì chúng ta hoàn toàn có một cách để lách được hạn chế này. Hãy cùng nhau xem qua slide bên dưới, đã được lấy ra từ [bài talk](https://www.youtube.com/watch?v=Obt-vMVdM8s) của [David Beazley](http://www.dabeaz.com/) ([slides](https://files.speakerdeck.com/presentations/619afb93e83c4ea795469fe1438d72b2/UnderstandingGIL.pdf))


<div>
    <p align="center">
        <img src="/personal-blog/assets/hi_concurrent3.png" style="width: 450px;">
    </p>
</div>


Như bạn có thể nhìn thấy, khi một thread thực hiện công việc của nó và nếu tồn tại các I/O opreations (chẳng hạn như đọc-ghi file), thì tại thời điểm đó GIL được giải phóng và nếu chương trình của chúng ta có đang thực hiện chạy multi-threading thì ngay lập tức một thread khác sẽ được chạy và cứ thế... cho đến khi mọi task trong tất cả các thread được thực hiện xong.

## Threading, Multiprocessing and Concurrent.futures

Thật ra, module concurrent.futures chỉ là một high-level module. Nó dựa hoàn toàn trên hai module khác của Python đó là [threading](https://docs.python.org/3/library/threading.html) và [multiprocessing](https://docs.python.org/3/library/multiprocessing.html). Nó cho phép những người mới bắt đầu làm quen với concurrent programming có một công cụ để có thể chơi với việc xử lý task đa luồng hoặc tận dụng tính đa lõi của những CPU đời mới một cách dễ dàng và nhanh chóng. Tuy vậy, đổi lại thì chúng ta sẽ không có thể tác động sâu vào bên trong lõi của hai module kia trong những tình huống phức tạp hơn.


## Some another examples

```python
import concurrent.futures
import urllib.request


URLS = ['https://dantri.com.vn/',
        'https://vnexpress.net/',
        'https://kenh14.vn/',
        'https://zingnews.vn/',
        'https://genk.vn/']


# Retrieve a single page and report the URL and contents
def load_url(url, timeout):
    with urllib.request.urlopen(url, timeout=timeout) as conn:
        return conn.read()


# We can use a with statement to ensure threads are cleaned up promptly
with concurrent.futures.ThreadPoolExecutor(max_workers=5) as executor:
    # Start the load operations and mark each future with its URL
    future_to_url = dict()

    for url in URLS:
        future = executor.submit(load_url, url, 60)
        future_to_url.update({future: url}) # (*)

    for future in concurrent.futures.as_completed(future_to_url):
        url = future_to_url[future]
        try:
            data = future.result()
        except Exception as exc:
            print('%r generated an exception: %s' % (url, exc))
        else:
            print('%r page is %d bytes' % (url, len(data)))
```

Ví dụ trên được lấy từ [trang documentation của Python](https://docs.python.org/3.7/library/concurrent.futures.html#threadpoolexecutor-example), được tôi có chỉnh sửa lại một chút để rõ ràng hơn cho người đọc. Nhiệm vụ của chính của hàm `load_url()` là thực hiện các GET request đến một số trang báo mạng nổi bật ở Việt Nam và sau khi nhận được response thì in ra độ dài byte của response từ mỗi trang. Đương nhiên là ví dụ này được chạy hoàn toàn bất đồng bộ.

Ở ví dụ này, chúng ta sử dụng hai function khác đó là [.submit()](https://docs.python.org/3.7/library/concurrent.futures.html#threadpoolexecutor-example) và [.as_completed()](https://docs.python.org/3.7/library/concurrent.futures.html#threadpoolexecutor-example). Function đầu tiên thuộc về subclass ThreadPoolExecutor, còn cái sau là module-level function. Như ở ví dụ trước đó của chúng ta, function .map() được sử dụng để chạy concurrent. Có một sự khác biệt ở function này đó là nó chỉ cho phép chúng ta sử dụng duy nhất một function chó các data khác nhau, hoàn toàn giống cách mà .map() built-in method của Python được sử dụng. Trong khi đó, ở ví dụ hiện tại, với việc sử dụng .submit(), chúng ta hoàn toàn có thể chạy thông qua một vòng lặp for và thực hiện pass vào các function khác nhau mà chúng ta muốn thực hiện. Như vậy nó sẽ linh hoạt hơn. Dẫu vậy thì ở ví dụ này, chúng ta chỉ sử dụng duy nhất một function cho việc chạy bất đồng bộ đó là `load_url()`. Ngoài ra, giá trị trả về của function .submit() sẽ là một đối tượng [future](https://docs.python.org/3.7/library/concurrent.futures.html#future-objects), về cơ bản là một đối tượng mà cho phép chúng ta có giữ được **trạng thái** cũng như **kết quả** sau khi mà task được thực hiện xong trong cơ chế bất đồng bộ của module futures.


Ở  dòng (\*) trong snippet code trên, ta tạo ra một dictionary để mapping mỗi URL đến đối tượng future tương ứng của chúng cho để tiện cho việc kiểm soát được kết quả trả về cũng như là quá trình debug.

`.as_completed()` có lẽ là function quan trọng nhất ở đoạn này. Tại sao? một loạt các task được chạy async bên trong thread pool và chúng ta không biết trước được là task nào ở thread nào sẽ được thực hiện xong và nếu muốn lấy kết quả của bất cứ từ một future bất kỳ, ta cần phải gọi phương thức [.result()](https://docs.python.org/3.7/library/concurrent.futures.html#future-objects) và khi gọi function này, chương trình hiện tại sẽ phải bị trì hoãn đến khi nào trạng thái của future đó phải là *done* thì mới có thể tiếu tục bất kể future nào khác đã hoàn thành hay không. Và đó là lý do mà `.as_completed()` có mặt trong module này.

## Kết

Tôi sẽ tạm dừng bài blog ở đây. Chủ để về *Concurrent Programming* không phải một sớm một chiều chúng ta có thể hiểu toàn diện được, tuy nhiên, bài viết này như là một cái tóm tắt nhanh cơ bản về cách chúng ta có thể thực hiện CP mà không cần quá nhiều nỗ lực mà tôi đúc kết được sau khi vọc vạch với module concurrent.futures Python và đọc được từ quyển sách rất hay của *Luciano Ramalho* với tiêu đề là [Fluent Python](https://learning.oreilly.com/library/view/fluent-python/9781491946237/)


### References:
* [Fluent Python](https://learning.oreilly.com/library/view/fluent-python/9781491946237/)
* [David Beazley's slide](https://files.speakerdeck.com/presentations/619afb93e83c4ea795469fe1438d72b2/UnderstandingGIL.pdf)
* [Parallel Programming with Python](https://books.google.com.vn/books?id=bE_lAwAAQBAJ&printsec=frontcover&dq=parallel+programming+with+python&hl=en&sa=X&ved=2ahUKEwi_p-re0InxAhWLwZQKHd6mDwcQ6AEwAXoECAgQAg#v=onepage&q=parallel%20programming%20with%20python&f=false)
