---
layout: page
title: About
permalink: /about/
---

Bùi Gia Huy == hosjiu (*tên đã được mã hoá theo quy tắc dịch mỗi ký tự trong tên sang phải một đơn vị phím.*)

*Dropout B.S*

#### Interestings:
* *Math*
* *ML theory*

#### Hobbies:
* *Music*
* *Speed*
* *Movie*
* *Hacking*

### **Nothing ... just do it.**

___

_Template của blog dựa trên tác giả [Andrej Karpathy](http://karpathy.github.io/)_
