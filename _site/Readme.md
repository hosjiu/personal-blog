# My blog
This is my blog, uses [Jekyll](http://jekyllrb.com/)
* _Template: [Andrej Karpathy](http://karpathy.github.io/)_

## How to run?
**$ bunle exec jekyll serve** _(auto reload)_

### For references
1. https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html
2. https://jekyllrb.com/docs/step-by-step/01-setup/
